import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component'
import { RelationComponent } from './relation/relation.component';
import { RelationFormComponent } from './relation/relationform.component';


const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'relation',
    component: RelationComponent
  },
  {
    path: 'relationform/:id',
    component: RelationFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
