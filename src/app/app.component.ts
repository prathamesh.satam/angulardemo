import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'client';
  local: string = '';
  constructor() {
    this.title = localStorage.getItem('ID');
    console.log(this.title);
  }
}
