import { Component, OnInit } from '@angular/core';
import gql from "graphql-tag";
import { Apollo } from "apollo-angular";
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-relation',
  templateUrl: './relation.component.html',
  styleUrls: ['./relation.component.sass']
})
export class RelationComponent implements OnInit {
  relations: Array<any> = [];
  constructor(private apollo: Apollo, private router: Router) {
    if (localStorage.getItem('ID') == null) {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
    this.getRelations();
  }

  getRelations() {
    const getRegistrations = gql`
      {
        GetAllRelations {
          id
          firstName
          lastName
          email
          country
          city
          eorino
          postalcode
          relationtype
        }
      }
    `;

    this.apollo
      .watchQuery({
        query: getRegistrations,
      })
      .valueChanges
      .pipe(map((result: any) => result.data.GetAllRelations))
      .subscribe(data => {
        this.relations = data;
      });
  }

  onEdit(id) {
    this.router.navigate(['./relationform/' + id]);
  }
}
