import { Component, OnInit } from '@angular/core';
import gql from "graphql-tag";
import { Apollo } from "apollo-angular";
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-relation',
    templateUrl: './relationform.component.html'
})
export class RelationFormComponent implements OnInit {
    relations: Array<any> = [];
    reltype: Array<any> = [];
    country: Array<any> = [];
    relationid: Number = 0;
    Msg: String = '';
    constructor(private apollo: Apollo, private router: Router, private route: ActivatedRoute) { }

    ngOnInit() {
        let id = this.route.snapshot.paramMap.get('id');
        const getRegistration1 = gql`
        {
            GetRelationById(id:${id}){
              id
              firstName
              lastName
              dob
              email
              username
              password
              country
              state
              city
              postalcode
              relationtype
              telephone
              mobile
              eorino
              website
              address
            }GetAllCountry{
                countryid
                countryvc
            }GetAllRelationtype{
                relationtypeid
                relationvc
            }       
          }
    `;
        this.apollo
            .watchQuery({
                query: getRegistration1,
                fetchPolicy: "network-only"
            })
            .valueChanges
            .pipe(map((result: any) => result.data))
            .subscribe(data => {
                console.log(data);
                this.relations = data.GetRelationById;
                this.country = data.GetAllCountry;
                this.reltype = data.GetAllRelationtype;
                this.relationid = data.GetRelationById.id;
            });
    }

    saveRelation(formData) {
        console.log(formData.firstName, 'lastname');
        console.log(formData.email, 'email');
        let relationarr = {
            id: this.relationid,
            firstName: formData.firstName,
            lastName: formData.lastName,
            email: formData.email,
            address: formData.address,
            postalcode: formData.postalcode
        }
        const updateRelation = gql`
            mutation updateRelation(
                $id: ID!
                $firstName: String!
                $lastName: String!
                $email: String!
                $address: String!
                $postalcode: String!
            ) 
            {
                updateRelation(
                    id: $id
                    firstName: $firstName
                    lastName: $lastName
                    email: $email
                    address: $address
                    postalcode: $postalcode
                ) {
                    id
                }
            }
        `;
        this.apollo
            .mutate({
                mutation: updateRelation,
                variables: relationarr
            })
            .subscribe(
                ({ data }) => {
                    console.log("got editdata", data);
                    this.Msg = "Success";
                },
                error => {
                    console.log("there was an error sending the query", error);
                }
            );
    }
}