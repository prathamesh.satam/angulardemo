import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import gql from "graphql-tag";
import { Apollo } from "apollo-angular";
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.sass']
})
export class ActionsComponent implements OnInit {
  header: string = "My";
  method: string = "";
  actions: Array<any> = [];
  constructor(private apollo: Apollo, private router: Router, private route: ActivatedRoute) {
    this.method = this.route.snapshot.paramMap.get('check');
    if (this.method == 'company') {
      this.companyactions();
    } else {
      this.myactions();
    }
  }

  ngOnInit() {

  }
  myactions() {
    this.header = "Company";
    let id = localStorage.getItem('login_id');
    const getMyActions = gql`
      {
        GetMyActions(id:1) {
          id
          client{
            clientname
          }
          contractno
          program
          action
          escalation1
          escalation2
        }
      }
    `;
    this.apollo
      .watchQuery({
        query: getMyActions,
      })
      .valueChanges
      .pipe(map((result: any) => result.data.GetMyActions))
      .subscribe(data => {
        console.log(data);
        //this.actions = data;
      });
  }

  companyactions() {
    this.header = "Company";
    const getAllActions = gql`
      {
        GetAllActions {
          id
          client{
            clientname
          }
          contractno
          program
          action
          escalation1
          escalation2
        }
      }
    `;
    this.apollo
      .watchQuery({
        query: getAllActions,
      })
      .valueChanges
      .pipe(map((result: any) => result.data.GetAllActions))
      .subscribe(data => {
        this.actions = data;
      });
  }
}
