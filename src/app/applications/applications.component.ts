import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RouteDailogueComponent } from './routedailogue.component';
import gql from "graphql-tag";
import { Apollo } from "apollo-angular";
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.sass']
})
export class ApplicationsComponent implements OnInit {
  animal: string;
  name: string;
  applications: Array<any> = [];
  constructor(private dialog: MatDialog, private route: Router, private apollo: Apollo) { }

  ngOnInit() {
    this.getList();
  }

  openDialog() {

    const dialogRef = this.dialog.open(RouteDailogueComponent, { width: '60%' });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

  getList() {
    const getApplications = gql`
      {
        GetAllApplications {
          id
          clientid{
            clientname
          }
          GOTS
          OCS
          CCS
          RCS 
          GRS 
          insertdate
          insertedby
          program
          office{
            countryvc
          }
        }
      }
    `;

    this.apollo
      .watchQuery({
        query: getApplications,
      })
      .valueChanges
      .pipe(map((result: any) => result.data.GetAllApplications))
      .subscribe(data => {
        this.applications = data;
      });
  }
}
