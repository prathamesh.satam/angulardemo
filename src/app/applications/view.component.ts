import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DailogueComponent } from './dailogue.component';

@Component({
    selector: 'app-applications',
    templateUrl: './view.component.html',
    styleUrls: ['./applications.component.sass']
})

export class ApplicationsViewComponent implements OnInit {
    animal: string;
    name: string;
    constructor(private dialog: MatDialog) { }

    ngOnInit() {
    }

    openDialog() {

        const dialogRef = this.dialog.open(DailogueComponent, {
            data: { name: this.name, animal: this.animal }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.animal = result;
        });
    }
}
