import { Component, OnInit } from '@angular/core';
import gql from "graphql-tag";
import { Apollo } from "apollo-angular";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

class Login {
  constructor(
    public firstName: string = "",
    public lastName: string = "",
    public dob: NgbDateStruct = null,
    public email: string = "",
    public password: string = "",
    public country: string = "Select country"
  ) { }
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})

export class LoginComponent implements OnInit {
  newarr = <any>Array;
  //newarr2 = <any>Array;
  errormsg: string = '';
  captcha: string = '';
  Username_VC: string = '';
  Password_VC: string = '';
  constructor(private apollo: Apollo, private router: Router) {
    if (localStorage.getItem('ID') != null) {
      localStorage.clear();
      this.router.navigate(['']);
    }
    let newarr2= [];
  }

  ngOnInit() {
    //this.checkLogin();
  }
  checkLogin(formData) {
    console.log(formData);
    if (formData.Username_VC == '' || formData.Password_VC == '') {
      this.errormsg = "Fill all fields";
    }
    else if (formData.captcha == '') {
      this.errormsg = 'Enter captcha';
    }
    else {
      console.log(formData.Username_VC);
      const getRegistration = gql`
      {
        CheckLogin(username:"${formData.Username_VC}",password:"${formData.Password_VC}") {
          id
          firstName
          lastName
          dob
          email
          country
        }
      }
    `;
      console.log(getRegistration)
      this.apollo
        .watchQuery({
          query: getRegistration,
          fetchPolicy: "network-only"
        })
        .valueChanges
        .pipe(map((result: any) => result.data.CheckLogin))
        .subscribe(data => {
          if (data != null) {
            localStorage.setItem('ID', data.id);
            location.href = './relation';
          } else {
            this.errormsg = "Incorrect username or password."
          }
        });
    }
  }

}
