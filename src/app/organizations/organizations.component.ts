import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.sass']
})
export class OrganizationsComponent implements OnInit {
  method: string = '';
  constructor(private route: ActivatedRoute) {
    this.method = this.route.snapshot.paramMap.get('check');
    if (this.method == 'createnew') {
      this.createnew();
    }
  }

  ngOnInit() {
  }

  createnew() {

  }

}
