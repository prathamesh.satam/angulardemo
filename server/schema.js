const schema = `
# declare custom scalars for date as GQDate
scalar GQDate
# registration type



type Country {
    id: ID!
    countryvc: String
}
type Relation {
    id: ID!
    firstName: String
    lastName: String
    address: String
    postalcode : String
    relationtype : String
    eorino : String
    country : Country!
    state : String
    city : String
    telephone : String
    mobile : String
    dob: GQDate
    email: String
    website : String
    username: String
    password: String
}
type relationtype {
    relationtypeid : ID!
    relationvc : String
}

type Query {    
    # Get All countries
    GetAllCountry(limit: Int) : [Country]

    # Get All relation types
    GetAllRelationtype(limit: Int) : [relationtype]

    # Check login
    CheckLogin(username : String, password : String) : Relation

    # Return all relations
    GetAllRelations(limit: Int): [Relation]

    # Return relation by id
    GetRelationById(id: ID!) : Relation   

    GetCountryById(id: ID!) : Country

}
type Mutation {
    updateRelation (
        id: ID!, 
        firstName: String,
        lastName: String,
        email: String, 
        address: String,
        postalcode: String
    ): Relation

}
`;

module.exports.Schema = schema;