const { GraphQLScalarType } = require("graphql");
const { find, filter } = require('lodash');
const relationType = require('./relationType.js');
console.log(relationType)
function convertDate(inputFormat) {
    function pad(s) {
        return s < 10 ? "0" + s : s;
    }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth()), d.getFullYear()].join("/");
}

// Define Date scalar type.

const GQDate = new GraphQLScalarType({
    name: "GQDate",
    description: "Date type",
    parseValue(value) {
        // value comes from the client
        return value; // sent to resolvers
    },
    serialize(value) {
        // value comes from resolvers
        return value; // sent to the client
    },
    parseLiteral(ast) {
        // value comes from the client
        return new Date(ast.value); // sent to resolvers
    }
});

// data store with default data
const relation = [
    {
        id: 1,
        firstName: "Johan",
        lastName: "Peter",
        address: "Chunabhatti",
        postalcode: "400022",
        relationtype: 1,
        eorino: "",
        country: 1,
        state: "Maharashtra",
        city: "Mumbai",
        telephone: "",
        mobile: "9869604614",
        dob: new Date("2014-08-31"),
        email: "johan@gmail.com",
        wesbite: "www.webiantechnologies.com",
        username: "Johan",
        password: "johan123"
    },
    {
        id: 2,
        firstName: "Johan2",
        lastName: "Peter",
        address: "Chunabhatti",
        postalcode: "400022",
        relationtype: 2,
        eorino: "",
        country: 3,
        state: "Maharashtra",
        city: "Mumbai",
        telephone: "",
        mobile: "9869604614",
        dob: new Date("2014-08-31"),
        email: "johan@gmail.com",
        wesbite: "www.webiantechnologies.com",
        username: "Johan",
        password: "johan123"
    }
];

//const relationtype = require('./relationType.js')
// console.log(relationtype)
const country = [
    {
        id: 1,
        countryvc: "India"
    },
    {
        id: 2,
        countryvc: "UK"
    },
    {
        id: 3,
        countryvc: "Netherlands"
    }
];

function check(user) {
    user.country = country.find(country => country.id == user.country)
    user.relationtype = relationtype.find(relation_type => relation_type.relationtypeid == user.relationtype).relationvc
}
function check2(arr) {
    let newRelation = [];
    arr.forEach(user => {
        newRelation.push({ ...user })
    });
    return newRelation;
}
const resolvers = {
    Query: {
        GetAllCountry: () => country, // return all registrations
        GetAllRelationtype: () => relationtype, // return all registrations
        GetAllRelations: () => {

            let newRelation = check2(relation);
            newRelation = newRelation.filter(user => {
                check(user)
                user.random = "123"
                return true
            });
            return newRelation;
        },
        // GetAllRelations: () => relation, // return all registrations
        GetCountryById: (_, { id }) =>
            country.find(country => country.id == id),
        CheckLogin: (_, { username, password }) =>
            relation.find(
                relation => relation.username == username
            ) &&
            relation.find(
                relation => relation.password == password
            ), // check login
        GetRelationById: (_, { id }) => {
            let newRelation = check2(relation);
            newRelation = newRelation.find(user => {
                check(user)
                return user.id == id
            })
            return newRelation;
        }
        // return relation by id
    },
    Mutation: {
        // // create a new registration
        // createRegistration: (root, args) => {
        //     // get next registration id
        //     const nextId =
        //         registrations.reduce((id, registration) => {
        //             return Math.max(id, registration.id);
        //         }, -1) + 1;
        //     const newRegistration = {
        //         id: nextId,
        //         firstName: args.firstName,
        //         lastName: args.lastName,
        //         dob: args.dob,
        //         email: args.email,
        //         password: args.password,
        //         country: args.country
        //     };
        //     // add registration to collection
        //     registrations.push(newRegistration);
        //     return newRegistration;
        // }, // delete registration by id
        // deleteRegistration: (root, args) => {
        //     // find index by id
        //     const index = registrations.findIndex(
        //         registration => registration.id == args.id
        //     );
        //     // remove registration by index
        //     registrations.splice(index, 1);
        // }, // update registration
        updateRelation: (root, args) => {
            // find index by id
            const index = relation.findIndex(
                relation => relation.id == args.id
            );
            relation[index].firstName = args.firstName;
            relation[index].lastName = args.lastName;
            relation[index].email = args.email;
            relation[index].address = args.address;
            relation[index].postalcode = args.postalcode;
            return relation[index];
        }
    },
    GQDate
};

module.exports.Resolvers = resolvers;
