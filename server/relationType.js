const relationType = [
    {
        relationtypeid: 1,
        relationvc: "Buyer"
    },
    {
        relationtypeid: 2,
        relationvc: "Supplier"
    },
    {
        relationtypeid: 3,
        relationvc: "Port Authority"
    },
]

module.exports = relationType;
